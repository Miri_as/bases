FIRST_DIGIT = '0'
LAST_DIGIT = '9'
FIRST_CHARACTER = 'A'
LAST_CHARACTER = 'Z'
BIGGEST_BASE = 36


def filter_function(sequence):
    digit_list = list(map(chr, range(ord(FIRST_DIGIT), ord(LAST_DIGIT) + 1)))
    character_list = list(map(chr, range(ord(FIRST_CHARACTER), ord(LAST_CHARACTER) + 1)))
    if sequence in digit_list or sequence in character_list:
        return True
    else:
        return False


def create_letters_list(base):
    if base <= int(LAST_DIGIT):
        sequence = list(map(chr, range(ord(FIRST_DIGIT), ord(str(base)))))
    else:
        sequence = list(map(chr, range(ord(FIRST_DIGIT), ord(LAST_CHARACTER) + 1)))
        sequence = list(filter(filter_function, sequence))
        sequence = sequence[:base]
    return sequence


def is_valid(numAsString, base):
    letters_list = create_letters_list(base)
    for letter in numAsString:
        if letter not in letters_list:
            return False
    return True


def base_to_decimal(numAsString, srcBase, letters_list):
    sol = 0
    for i, x in enumerate(reversed(numAsString)):
        sol += letters_list.index(x) * pow(srcBase, i)
    return sol


def decimal_to_base(decimalNumber, dstBase, letters_list):
    base_num = ""
    while decimalNumber > 0:
        decimalNumber, remainder = divmod(decimalNumber, dstBase)
        base_num += letters_list[remainder]
    base_num = base_num[::-1]  # To reverse the string
    return base_num


def converting(numAsString, srcBase, dstBase):
    dstSolution = []
    if not is_valid(numAsString, srcBase):
        return dstSolution
    else:
        if srcBase != 10:  # need to convert to decimal
            letters_list = create_letters_list(srcBase)
            decimalNumber = base_to_decimal(numAsString, srcBase, letters_list)
            letters_list = create_letters_list(dstBase)
            dstSolution = decimal_to_base(decimalNumber, dstBase, letters_list)
        else:
            letters_list = create_letters_list(dstBase)
            dstSolution = decimal_to_base(int(numAsString), dstBase, letters_list)

        return dstSolution


def printing(numAsString, srcBase, dstBase, result_converted, is_valid_src_base, is_valid_dst_base):
    print("Result For (" + numAsString + "," + str(srcBase) + "," + str(dstBase) + "):\n")
    if not is_valid_src_base:
        print("\tSource base " + str(srcBase) + " is not a legal base\n")
    elif not is_valid_dst_base:
        print("\tDest base " + str(srcBase) + " is not a legal base\n")
    elif len(result_converted) == 0:
        print("\tThe number " + numAsString + " is not a legal number in base " + str(srcBase) + "\n")
    else:
        print("\t" + numAsString + " is a valid number in base " + str(srcBase) + "\n")
        print("\tIts representation in destination base (" + str(dstBase) + ") is: " + result_converted + "\n")


def check_base(base):
    return base <= BIGGEST_BASE


def main():
    try:
        numAsString = input("Please enter string to convert:\n")
        srcBase = int(input("Please enter src base:\n"))
        dstBase = int(input("Please enter dst base:\n"))
        result_converted = ""
        if not check_base(srcBase):
            printing(numAsString, srcBase, dstBase, result_converted, False, True)
        elif not check_base(dstBase):
            printing(numAsString, srcBase, dstBase, result_converted, True, False)
        else:
            numAsString = numAsString.upper()
            result_converted = converting(numAsString, srcBase, dstBase)
            printing(numAsString, srcBase, dstBase, result_converted, True, True)
    except ValueError:
        print("Invalid input.\nPlease enter a string, and two numbers.\n")
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()
