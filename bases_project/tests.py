from unittest import TestCase
import main


class Tests(TestCase):

    def test_is_valid(self):
        assert main.is_valid("4D2", 16)
        assert not main.is_valid("4D2", 2)

    def test_base_to_decimal(self):
        srcBase = 16
        letters_list = main.create_letters_list(srcBase)
        assert main.base_to_decimal("4D2", srcBase, letters_list) == 1234
        assert main.base_to_decimal("4D2", srcBase, letters_list) != 1235
        srcBase = 2
        letters_list = main.create_letters_list(srcBase)
        assert main.base_to_decimal("0110", srcBase, letters_list) == 6

    def test_decimal_to_base(self):
        letters_list = main.create_letters_list(16)
        assert main.decimal_to_base(1234, 16, letters_list) == "4D2"

    def test_converting(self):
        assert main.converting("4D2", 16, 10) == '1234'

    def test_check_base(self):
        assert main.check_base(15)
        assert not main.check_base(48)
